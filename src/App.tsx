import React from 'react';
import './App.css';
import { Counter, Button } from "npm-app-jennifer";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Counter />
        <br />
        <Button props={{label:"Hello"}} />
      </header>
    </div>
  );
}

export default App;
